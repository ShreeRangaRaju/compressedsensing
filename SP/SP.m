function [Xest,support] = SP(A, b, K)


r0 = b;
iter = 1;

abso = abs(A'*b);
[maxVal,maxInd] = sort(abso,'descend');
I0 = (maxInd(1:K));

r0 = b-(A(:,I0)*pinv(A(:,I0))*b);
currentNorm = norm(r0);
prevNorm = 0;

while 1
    iter = iter+1;
    
    prevI0 = I0;
    abso2 = abs(A'*r0);
    [maxVal2, maxInd2] = sort(abso2,'descend');
    Tow = union(I0,(maxInd2(1:K)));
    
    vTow = pinv(A(:,Tow))*b;
    [maxVal3, maxInd3] = sort(abs(vTow),'descend');
    I0 = Tow(maxInd3(1:K));
    
    r0 = b-(A(:,I0)*pinv(A(:,I0))*b);
    %prevNorm = currentNorm;
    currentNorm = norm(r0);
    
    if (currentNorm >= prevNorm)
        I0 = prevI0;
        break;
    end
    
    
end

[M,N] = size(A);
h = pinv(A(:,I0))*b;
Xest = zeros(N,1);
Xest(I0) = h;
support = I0;
