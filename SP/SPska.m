function [xIk, I0] = SPska(A, b, K)

[~, index] = sort(abs(A'*b), 'descend');
I0 = index(1:K); 
r0 = b - A(:,I0)*pinv(A(:,I0))*b;
currResNorm = norm(r0,2);
[M,N] = size(A);
iter = 1;
while 1
    iter = iter+1;                    
    [~, index1] = sort(abs(A'*r0), 'descend');
  
    I0 = union(I0, index1(1:K)); 
    xIk = zeros(N,1);
    xIk(I0) = pinv(A(:,I0))*b;
    [~,index2] = sort(abs(xIk), 'descend');
    I0 = index2(1:K)';
   
    r0 = b-A(:,I0)*pinv(A(:,I0))*b;
    prevResNorm = currResNorm;
    currResNorm = norm(r0,2);
    if(prevResNorm <= currResNorm) 
        break;
    end  
   
end
xIk = zeros(N,1);
xIk(I0) =  pinv(A(:,I0))*b;
end