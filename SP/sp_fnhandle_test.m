% Written by Shree Ranga Raju NM,
% Statistical signal processing lab,
% Dept of Electrical Communication Engineering,
% Indian Institute of Science, Bangalore

function [xest,r] = sp_fnhandle_test(A,b,k)

if iscell(A)
    Largescale = true;
    Af = A{1};
    At = A{2};
end

% Initialization
%r = b
RHS = b;
Ar = At(RHS);
N = size(Ar,1);
M = size(RHS,1);
xest = zeros(N,1);
maxiter = M;

[~,maxind] = sort(abs(Ar),'descend');
Ik = maxind(1:k);

x_warmstart = xest(Ik);

cg_tol = 1.0e-3;
cg_maxit = 50;

if Largescale
    LSQR_ALG    = @(RHS,Afcn,x_warmstart) lsqr(Afcn,RHS,cg_tol,cg_maxit,[],[],x_warmstart );
end

if Largescale
    Afcn = @(x,version) partialA( N, Ik, Af, At, x, version );
    [x_T,~] = LSQR_ALG(RHS,Afcn,x_warmstart);
end

xest       = 0*xest;
xest( Ik ) = x_T;



r = b-Af(xest);
currentnorm = norm(r);

% for kk = 1:maxiter
while(1)
    
    prevIk = Ik;
    
    Ar = At(r);
    
    [~,maxind1] = sort(abs(Ar),'descend');
    
    ind_new     = maxind1(1:k);
    
    T = union(Ik,ind_new);
    
    x_warmstart = xest(T);
    
    %     if Largescale
    %     LSQR_ALG    = @(RHS,Afcn,x_warmstart) lsqr(Afcn,RHS,cg_tol,cg_maxit,[],[],x_warmstart );
    % end
    %
    if Largescale
        
        Afcn = @(x,version) partialA( N, T, Af, At, x, version );
        [x_T,~] = LSQR_ALG(RHS,Afcn,x_warmstart);
    end
    
    
    cutoff  = findCutoff(x_T, k);
    Tk      = find( abs(x_T) >= cutoff );
    Ik   = T(Tk);
    
    
    
    
    
    xest       = 0*xest;
    xest( Ik ) = x_T(Tk);
    
    
    
    r = b-Af(xest);
    
    prevnorm = currentnorm;
    currentnorm = norm(r);
    
    if (prevnorm <= currentnorm)
        Ik = prevIk;
        break;
    end;
    
    
end



end % main function end

function z = partialA( N, T, Af, At, x, version )


switch lower(version)
    case 'notransp'
        %if (transp_flag == 'notransp')
        %zero-pad:
        
        y   = zeros(N,1);
        y(T)    = x;
        z       = Af(y);
        
    case 'transp'
        %elseif (transp_flag == 'transp')
        
        y   = At(x);
        % truncate:
        z   = y(T);
end
end % -- end of subfuction

function tau = findCutoff( x_T, k )

x_T   = sort( abs(x_T),'descend');


tau  = x_T(k);


end



