clear all; close all; clc
% Purpose   : Simulate LAOMP
% Reference : Backtracking-Based Matching Pursuit Method for Sparse Signal Reconstruction-H.Huang,A.Makur(IEEE SPLetters July 2011)
% Author    : Sooraj K. Ambat
% Email     : sooraj@ece.iisc.ernet.in
%             sooraj.k.ambat@gmail.com
% Address   : Ph.D. Scholar,
%             Statistical Signal Processing Lab,
%             Indian Institute of Science, Bangalore
% Date      : 18 July 2011
%addpath /home/sooraj/Research/CompressedSensing/MatlabCodes/laOMP; % to Use pOMP
%addpath /home/sooraj/Research/CompressedSensing/CSMatlabToolbox/l1magic-1.1/Optimization %to use the l1-magic toolbox
addpath /srv/home/Student1/sooraj/Programs/CSMatlabToolbox/l1magic-1.1/Optimization %to use the l1-magic toolbox in Sankhya
addpath /srv/home/Student1/sooraj/Programs/laOMPSankhyaSimulation % to Use pOMP in Sankhya
%% signal generation
n = 256; % signal dimension
m = 128; % no. of sensors

%% Generating Measurement matrix
A = randn(m,n)*1/m;
for j=1:n
    v = norm(A(:,j),2);
    A(:,j) = A(:,j)/v;
end
sparsityLevel = 5:5:90; % sparsity level


pOMPsuccessCount = zeros(length(sparsityLevel),1);
BPsuccessCount =  zeros(length(sparsityLevel),1);
baOMPsuccessCount =  zeros(length(sparsityLevel),1);
maxError = 1e-3;

for i=1:length(sparsityLevel) % run for different sparsity
    k = sparsityLevel(i)
    noSimulations = 500;
    for t=1:noSimulations
        % non-zero values are drawn from Gaussian distribution with zero mean and
        % unit variance
        positions  = randperm(n); positions = positions(1:k); % Location of non-zero rows in X
        x = zeros(n,1); x(positions,:) = randn(k,1);    % The k-row-sparse solution
        
        b = A*x; % Measurements
        
        
        %% pOMP estimation
        p = 2; % use l2-norm
        pOMPRes=  pOMP(A, b, k, p);
        pOMP_estX = zeros(n,1);
        pOMP_estX(pOMPRes) = pinv(A(:,pOMPRes))*b;
        err = max(abs(x-pOMP_estX));
        if(err < maxError)
            pOMPsuccessCount(i) = pOMPsuccessCount(i) + 1;
        end
        %% Basis Pursuit (uses l1-magic toolbox)
        x0 = A'*b; % initial guess = minimum energy
        BP_estX = l1eq_pd(x0, A, [], b, 10^(-3));
        err = max(abs(x-BP_estX));
        if(err < maxError)
            BPsuccessCount(i) = BPsuccessCount(i) + 1;
        end
        
        %% baOMP estimation
        mu1 = 0.4; mu2 = 0.6; baOmP_maxError = 10^(-6); maxIter = m;
        [baOMP_estX, baOMP_estSupport] = baOMP(A, b, mu1, mu2, baOmP_maxError, maxIter);
        baOMP_estSupport = sort(baOMP_estSupport);
        actualSupport = sort(positions);
        err = max(abs(x-baOMP_estX));
        if(err < maxError)
            baOMPsuccessCount(i) = baOMPsuccessCount(i) + 1;
        end
    end
end
fileName = sprintf('baOMPOutputGaussian%s.mat', date)
save(fileName, 'pOMPsuccessCount', 'BPsuccessCount', 'baOMPsuccessCount', 'sparsityLevel', 'maxError', 'm','n', 'noSimulations', 'mu1', 'mu2', 'baOmP_maxError');