function [estX, estSupport] = baOMP(A, b, mu1, mu2, maxError, maxIter)
% Backtracking-based Adaptive OMP Implementation-Sooraj
%

% Inputs
%   A       : Sensing Matrix (size mxn)
%   b       : Measurement Vector (size mx1); b = Ax
%   mu1     : atom adding constant threshold in [0,1]
%   mu2     : atom deleting constant threshold in [0,1]
%   maxError: convergence threshold to stop iterations
%   maxIter : maximum no. of iterations
% Outputs
%   estX    : estimated non-zero values
%   estSupport  : the support of x
% Purpose   : calculate the unknown support of b = Ax
% Reference : Backtracking-Based Matching Pursuit Method for Sparse Signal Reconstruction-H.Huang,A.Makur(IEEE SPLetters July 2011)
% Author    : Sooraj K. Ambat
% Email     : sooraj@ece.iisc.ernet.in
%             sooraj.k.ambat@gmail.com
% Address   : Ph.D. Scholar,
%             Statistical Signal Processing Lab,
%             Indian Institute of Science, Bangalore
% Date      : 17 July 2011
% Note      : Use it only for SMV case!

[m,n] = size(A);
[~,cols] = size(b);
if(cols ~= 1)
    error('This version supports only SMV');
end

% initialization
Ik = [];            % estimated support set
currentResidue = b; % initial residual
%candidateSet = [1:n];  % candidate set
%deleteSet = [];

for iterNo =1:maxIter
    iterNo;
    %% Preliminary Selection (find atoms with large correlation)
    val = abs(A'*currentResidue);
    val(Ik) = 0; % we are not interested in these locations!
    maxCorrelation = max(val);
    %val = abs(A(:, candidateSet)'*currentResidue);
    index = find(val >= mu1*maxCorrelation);
    if(length(index) + length(Ik) <= m)
        candidateSet = index';
    else
        candidateSet = index';
        %         index
%         xxxx=1 % fill it later
%         size(index) 
%         length(Ik)
%         error('hahaha');
    end
    
    %% Final Selection (find candidates for deletion, if any)
    unionSet = [candidateSet, Ik];
    estX = pinv(A(:,unionSet))*b;
    maxVal =  max(abs(estX(1:length(candidateSet)))); % choose the maximum among the candidate set
    deleteSet = unionSet(find(abs(estX) < mu2*maxVal)); % delete unionSet member which falls below the threhold
    
    %% Update (update the residue and residue error)
    Ik =  setdiff(unionSet, deleteSet);
    estX = pinv(A(:,Ik))*b;
    currentResidue = b - A(:,Ik)*estX;
    residueError = sqrt(sum(currentResidue.^2)); % norm of the residue
    if(residueError < maxError)
%         residueError;
        break;
    end
end
estSupport = Ik;
estX = zeros(n,1);
estX(estSupport) = pinv(A(:,Ik))*b;
