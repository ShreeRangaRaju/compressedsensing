function [Jset] = pOMP(A, b, k, p)
% p-norm Orthogonal Matching Pursuit Algorithm Implementation-Sooraj
%

% Inputs
%   A       : Sensing Matrix (size mxn)
%   b       : Measurement Vector (size mx1) b = Ax
%   k       : sparsity level
%   p       : norm used (by default, p =2)
% Outputs
%   Jset    : the support of x
% Purpose   : Implement the p-OMP Algorithm for MMV Compressive Sensing
% Reference : Subspace-Augmented MUSIC for Joint Sparse
% Recovery-K.Lee(arXiv Jan 2011), Page: 9
% Author    : Sooraj K. Ambat
% Email     : sooraj.k.ambat@gmail.com
% Address   : Ph.D. Scholar,
%             Statistical Signal Processing Lab, 
%             Indian Institute of Science, Bangalore
% Date      : 04 June 2011
% Note      : This is to be used only for SMV case

[m,n] = size(A);
[m1, N] = size(b);
if(m ~= m1)
    error('No. of Columns of A should match the no. of rows of B');
end
if(N ~= 1)
   error('Can be used  only for SMV case, b must be a vector'); 
end
if(nargin == 3) % default value for 'p'
   p =2; 
end

PAjPerp = eye(m); 
% fullSet = [1:n]'; % full values of 'n'
Jset = [];
for i=1:k
%    diffSet = setdiff(fullSet, Jset);
%     val = zeros(n,1);
%     for j=1:length(diffSet)
%        val(diffSet(j)) = norm(b'*PAjPerp*A(:,diffSet(j)),p);
%     end

    colProj = b'*PAjPerp*A;
    val = abs(colProj);     % (sum(abs(colProj).^p)).^(1/p);
    val(Jset) = 0;          % we are not interested in these values
    [~, index] = max(val);  % find index of the largest element
    Jset = [Jset index];    % update the support
    PAjPerp = eye(m)-A(:,Jset)*pinv(A(:,Jset)); 
end
% Jset
%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%