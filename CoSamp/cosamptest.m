 clc; close all; clear all;

%rng('default')
%ratio = 0;
%AMSCE = 0;
N = 10; % dimesion of signal
K = 2; % level of sparsity
%M = 7;
g = 0;
DiffValx = 1;
m1 = 7;
m2 = 7;

for M = m1:m2
    
    sumNormError = 0;
sumNormx = 0;
sumLen = 0;
    A = randn(M,N);
      for i = 1:N % normalizing the signal begins here
            r = A(:,i);
            nrm1 = norm(r);
            A(:,i) = r/nrm1;
      end
      
      
    for d = 1:DiffValx
        
        x=zeros(N,1); %initializing the signal
        position = randperm(N,K) %randomly chosen non-zero indices of x
        x(position) = randn(K,1);
        y = (A*x);
        b = awgn(y,1);
        
        [Xest, support] = OMP(A, b, K);   
        x
        Xest
      
        
        error = max(abs(Xest-x)); % error between maximum absolute value of estimated x and x
        diff = x-Xest; % the error between x and the estimated x
        normDiff = (norm(diff))^2; % norm of the error
        sumNormError = sumNormError+normDiff; %summation of all iterations
        sumNormx = sumNormx+(norm(x))^2; % summation of norm of all the values of x
        
         SamePos = intersect(position,support);
        len = length(SamePos);
      sumLen = sumLen+len;
       
        
        
        
       
    end
     
    
   ASCE(M) = ((K*DiffValx)-sumLen)/(K*DiffValx);
    
       
        
    
         
        ratio(M) = sumNormx/sumNormError;
        
        
        
end








