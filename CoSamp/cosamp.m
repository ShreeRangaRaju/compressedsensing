function[Xest,support] = cosamp(A, b, K)

r0 = b;
iter = 1;
I0 = [];
currentNorm = norm(r0);
previousNorm = 0;
[M,N] = size(A);


while 1
    
   iter = iter+1;
   
   [maxVal, maxInd] = sort(abs(A'*r0),'descend');
   I1 = maxInd(1:2*K);
   Tow = union(I0,I1);
   
   vTow = zeros(N,1);
   vTow(Tow) = pinv(A(:,Tow))*b;
   
   [maxVal1, maxInd1] = sort(abs(vTow),'descend');
   I2 = maxInd1(1:K);
   
   xhat = zeros(N,1);
   xhat(I2) = vTow(I2);
   
   r1 = b-A*xhat;
   
   
   currentNorm = norm(r1);
   
   if (currentNorm > previousNorm)
      break;
   end
   
   I0 = I1;
   r0 = r1;
  
   
end


Xest = zeros(N,1);
Xest = xhat;
support = I2;
