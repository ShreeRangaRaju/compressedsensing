function [xIk, Ik, noIter] = SPska1(A, b, K)
% Subspace Pursuit Algorithm
[~, index] = sort(abs(A'*b), 'descend');
Ik = index(1:K); % |Ik| = K
% update residue

rk = b - A(:,Ik)*pinv(A(:,Ik))*b;

currResNorm = norm(rk,2);
[M,N] = size(A);
maxIter = M;

for counter=1:maxIter
    prevIk = Ik;                    % storing prevIk value
    
    [~, index1] = sort(abs(A'*rk), 'descend');
    
    Ik = union(Ik, index1(1:K)'); % K<=|Ik|<=2K
    % choose best K
    xIk = zeros(N,1);
    
    xIk(Ik) = pinv(A(:,Ik))*b;
    
    [~,index2] = sort(abs(xIk), 'descend');
    Ik = index2(1:K)';
    % update residue
    rk = b-A(:,Ik)*pinv(A(:,Ik))*b;
    prevResNorm = currResNorm;
    currResNorm = norm(rk,2);
    if(prevResNorm <= currResNorm) % check halting condition
        Ik = prevIk;
        break;
    end
end
xIk = zeros(N,1);
xIk(Ik) =  pinv(A(:,Ik))*b;

noIter = counter;
end

%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%