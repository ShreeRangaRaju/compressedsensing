clc; clear all; close all;

load('cosampgimodificationS10T10')
probcosamp = zeros(length(alpha),1);
probmodcosamp = zeros(length(alpha),1);
probgicosamp = zeros(length(alpha),1);

for i = 1:length(alpha)
    
    
    for k = 1:(S*T)

        
        if(err_cosamp_vect(k,i) < 0.01)
            probcosamp(i) = probcosamp(i) + 1
            
        end
        
    end
end


for i = 1:length(alpha)
    
    
    for k = 1:(S*T)
       
        
        if(err_modified_cosamp_vect(k,i) < 0.01)
            probmodcosamp(i) = probmodcosamp(i) + 1
            
        end
        
    end
end


for i = 1:length(alpha)
    
    
    for k = 1:S*T
        
        
        if(err_modified_gicosamp_vect(k,i) < 0.01)
            probgicosamp(i) = probgicosamp(i) + 1
            
        end
        
    end
end

plot(alpha,probcosamp/(S*T),'b-o');
hold on;
plot(alpha,probmodcosamp/(S*T),'m-+');
hold on;
plot(alpha,probgicosamp/(S*T),'r-*')
grid on;
legend('origcosamp','modifiedTenconCosamp','GICosamp')
xlabel 'M'
ylabel 'Probability of exact reconstruction (Threshold is 0.01)'
title 'Exact reconstruction rate of gaussian sparse sparse signal.  N = 500, S=T=10, K = 20'