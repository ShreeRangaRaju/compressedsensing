function [xIk, Ik, noIter] = Modified_CoSaMP(A, b, K, mu)

%% initialization
[M,N] = size(A);
Ik = [];
xIk = zeros(N,1);
rk = b;
currResNorm = norm(rk,2);
maxIter = M;
for counter=1:maxIter
    prevIk = Ik; %storing prevIk value
    prev_xIk = xIk;
    matchFilter = abs(A'*rk);
    % Identify large components
    [sortVal, index1] = sort(matchFilter, 'descend');
    % find dimension of search space adaptively
    index = index1(find(sortVal >= mu*sortVal(1)));
    len = length(index);
    if(length(index)>M-K)   % restricting the maximum size to M-K
        len = M-K;  
    end     
    
    % Merge Supports 
    Ik = union(Ik, index1(1:len)'); %in CoSaMP len = 2*K always
    % choose best k using least squares
    xIk_tmp = zeros(N,1);
    xIk_tmp(Ik) = A(:,Ik)\b;
    [~,index2] = sort(abs(xIk_tmp), 'descend');
    Ik = index2(1:K)';
    % estimate of the signal
    xIk = zeros(N,1);
    xIk(Ik) = xIk_tmp(Ik);  
    %update residue (CosaMP terminlology 'update current samples')
    rk = b-A*xIk;
    prevResNorm = currResNorm;
    currResNorm = norm(rk,2);
    if(prevResNorm <= currResNorm)
        Ik = prevIk;
        xIk = prev_xIk;
       % counter = counter-1;
        break;
    end    
end
noIter = counter;
end

%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
