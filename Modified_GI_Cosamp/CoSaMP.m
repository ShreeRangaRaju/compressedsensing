function [estX, estSupportSet, noIter] = CoSaMP(A, b, K)


% initialization
[M,N] = size(A);
estSupportSet = [];
estX = zeros(N,1);
rk = b;
currResNorm = norm(rk,2);
maxIter = M;
for counter=1:maxIter
    prev_estSupportSet = estSupportSet; %storing prevIk value
    prev_estX = estX;
    matchFilter = abs(A'*rk);
    % Identify large components
    [~, index1] = sort(matchFilter, 'descend');
    % Merge Supports
    interimSupportSet = union(estSupportSet, index1(1:2*K)'); % K<=|interimSupportSet|<=3K
    % choose best k using least squares
    interimEstX = zeros(N,1);
    interimEstX(interimSupportSet) = A(:,interimSupportSet)\b; 
    [~,index2] = sort(abs(interimEstX), 'descend');
    estSupportSet = index2(1:K)';
    % estimate of the signal
    estX = zeros(N,1);
    estX(estSupportSet) = interimEstX(estSupportSet);  
    %update residue (CosaMP terminlology 'update current samples')
    rk = b-A*estX;
    prevResNorm = currResNorm;
    currResNorm = norm(rk,2);
    if(prevResNorm <= currResNorm)
        estSupportSet = prev_estSupportSet;
        estX = prev_estX;
        break;
    end    
end
noIter = counter;
end

%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
