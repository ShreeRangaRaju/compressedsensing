function [xest,support] = gisp(A,b)

[m,n] = size(A);

cset = [];
dset = [];
fset = [];

inittrans = abs(A'*b);
maxval1 = max(inittrans);
ginib = giniindex(b);
threshold1 = ginib*maxval1;
fset = find(inittrans >= threshold1);
r = b-A(:,fset)*A(:,fset)\b;
maxerror = 10^-6;

for i = 1:m
    i;
    trans = abs(A'*r);
    trans(fset) = 0;
    maxval2 = max(trans);
    ginir = giniindex(r);
    threshold2 = ginir*maxval2;
    index = find(trans >= threshold2);
    cset = index';
    unionset = union(fset,cset);
    xest = A(:,unionset)\b;
    maxval3 = max(abs(xest(1:length(cset))));
    gixest = giniindex(xest);
    threshold3 = gixest*maxval3;
    threshold3Elements = find(abs(xest) < threshold3);
    dset = unionset(threshold3Elements);
    fset = setdiff(unionset,dset);
    xest = A(:,fset)\b; 
    r = b - A(:,fset)*xest;
     nrmr = norm(r);
    if(nrmr < maxerror) 
        break;
    end
    
end

support = fset; 

xest = zeros(n,1);

xest(support) = A(:,fset)\b;
    
    
    
    
