function [A] = generateMeasurementMatrix(TYPE, M, N)
% Generate Random Measurement Matrices for Compressed Sensing simulations
%

% Inputs
%   TYPE    : type of the random matrix required
%             'NormalizedStdGaussian' - Std Gaussian random matrix with normalized columns
%               'UnNormalizedStdGaussian' - Std Gaussian random matrix with un-normalized columns
%   M       : no. of rows of the matrix
%   N       : no. of columns of the matrix
% Outputs
%   A       : generated matrix (size:MxN)
%  
% Purpose   :
% Reference : 
% Author      	: Sooraj K. Ambat
% Email       	: sooraj@ece.iisc.ernet.in/sooraj.k.ambat@gmail.com
% Address      	: Ph.D. Scholar,
%                 Statistical Signal Processing Lab,
%                 Indian Institute of Science, Bangalore, 560012, India.
% Date       	: 20 Aug 2011
% Last Modified : 09 May 2012

% In the spirit of reproducible research, here we provide the Matlab codes
% used to generate the results given in our paper entitled "On Selection 
% of Search Space Dimension in Compressive Sampling Matching Pursuit", in 
% Proceedings of IEEE Region 10 Conference (TENCON), November 2012.
% Authors: Sooraj K. Ambat, Saikat Chatterjee, K.V.S. Hari.
% 
% If you use this code in your work, please cite our paper mentioned above.


% This script/program is released under the Commons Creative Licence
% with Attribution Non-commercial Share Alike (by-nc-sa)
% http://creativecommons.org/licenses/by-nc-sa/3.0/
% Short Disclaimer: this script is for educational purpose only.



if(strcmp(TYPE, 'NormalizedStdGaussian'))
    % Generate Measurement matrix
    A = randn(M,N)*1/M;
    A = normalizeColumns(A);
% %     for j=1:N
% %         v = norm(A(:,j),2);
% %         A(:,j) = A(:,j)/v; % normalizing the columns of A
% %     end    
else
    error('Unknown type');
end
end

