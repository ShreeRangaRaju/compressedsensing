% final program for checking of algorithms and to run 500 simulations

clc; clear all; close all;

RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

N = 256;
K  = 30;
M = 60:5:100;

maxError = 1e-3;

noSimulations = 500;




% OMPsuccessCount = zeros(length(M),1);
% SPsuccessCount = zeros(length(M),1);
% BPsuccessCount = zeros(length(M),1);
% fbp_bin_successCount30 = zeros(length(M),1);
% newgi_successCount = zeros(length(M),1)
tenconSpringSuccess= zeros(length(M),1);




for len = 1:length(M)
    m = M(len)
    
    A = randn(m,N);
    for i = 1:N % normalizing the signal begins here
        r = A(:,i);
        nrm1 = norm(r);
        A(:,i) = r/nrm1;
    end
    
    
    
    for t = 1:noSimulations
        x=zeros(N,1); %initializing the signal
        position = randperm(N,K); %randomly chosen non-zero indices of x
        x(position) = randn(K,1);
        
        gix  = giniindex(x);
        b = A*x;
        
        
        %newgitest
        [xest,support] = gicosamp(A,b,K,gix);
        err = max(abs(x-xest));
        if(err < maxError)
            tenconSpringSuccess(len) = tenconSpringSuccess(len)+1;
            
        end
        
%         %fbptest
%         alpha = 30; beta = 29;
%         [xest,support] = fbp(A,b,alpha,beta);
%         err = max(abs(x-xest));
%         if(err < maxError)
%             fbp_bin_successCount30(len) = fbp_bin_successCount30(len)+1;
%             
%         end
%         
%         
%         
%         
%         
%         %OMP Simulation
%         [xestOMP,suppotOMP] = OMP(A,b,K);
%         err = max(abs(x-xestOMP));
%         if(err < maxError)
%             OMPsuccessCount(len) = OMPsuccessCount(len) + 1;
%         end
%         
%         
%         %SP simulation
%         [xestSP,supportSP] = SPska1(A,b,K);
%         err = max(abs(x-xestSP));
%         if(err < maxError)
%             SPsuccessCount(len) = SPsuccessCount(len) + 1;
%         end
%         
%         % BP simulation
%         x0 = A'*b; % initial guess = minimum energy
%         BP_estX = l1eq_pd(x0, A, [], b, 10^(-3));
%         err = max(abs(x-BP_estX));
%         if(err < maxError)
%             BPsuccessCount(len) = BPsuccessCount(len) + 1;
%         end
        
        
    end
end

%save('NewGIbasedAlgoforDiffM')


