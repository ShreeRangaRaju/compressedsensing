function [estX, estSupportSet, noIter] = gicosamp(A, b, K,gix)


% initialization

[m,n] = size(A);

estSupportSet = [];

estX = zeros(n,1);



rk = b;

currResNorm = norm(rk,2);

maxIter = m;

for counter=1:maxIter
    
    prev_estSupportSet = estSupportSet; %storing prevIk value
    
    prev_estX = estX;
    
    trans = abs(A'*rk);
    
    % Identify large components
    [maxVal, index1] = sort(trans, 'descend');
    
    tmp = zeros(n,1);
    
    
    
    
    
    for i = 1:n
        
        tmp(i) = maxVal(i);
        gitmp = giniindex(tmp);
        if(gix > gitmp);
            break;
        end
        
        
    end
    
    
    candidateSet = index1(1:i)';
    
    
    % Merge Supports
    interimSupportSet = union(estSupportSet, candidateSet); % K<=|interimSupportSet|<=3K
    
    % choose best k using least squares
    interimEstX = zeros(n,1);
    
    interimEstX(interimSupportSet) = A(:,interimSupportSet)\b;
    
    [~,index2] = sort(abs(interimEstX), 'descend');
    
    estSupportSet = index2(1:K)';
    
    % estimate of the signal
    estX = zeros(n,1);
    
    estX(estSupportSet) = interimEstX(estSupportSet);
    
    %update residue (CosaMP terminlology 'update current samples')
    rk = b-A*estX;
    
    prevResNorm = currResNorm;
    
    currResNorm = norm(rk,2);
    
    if(prevResNorm <= currResNorm)
        estSupportSet = prev_estSupportSet;
        estX = prev_estX;
        break;
    end
end

noIter = counter;

end

%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
