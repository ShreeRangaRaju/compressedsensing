
function[xest,support] = fbp(A,b,alpha,beta)

[m,n] = size(A);


%Kmax = 70;
epsilon = 9*(10^-7);
xest = 0;

s = [];
r = b;
k = 0;

if(alpha > n)
    error('Alpha cannot be greater than No. of columns in matrix A')
end

for i = 1:m
    k = k + 1;
    trans = A'*r;
    trans(s) = 0;
    [~,ind1] = sort(abs(trans),'descend');
    tf = ind1(1:alpha);
    s = union(s,tf);
    xest = A(:,s)\b;
    [~,ind2] = sort(abs(xest),'ascend');
    t = ind2(1:beta);
    tb = s(t);
    s = setdiff(s,tb);
    xest = A(:,s)\b;
    r = b - A(:,s)*xest;
    if (norm(r) <= epsilon*norm(b))
        break;
    end
end
support = s;
xest = zeros(n,1);
xest(support) = A(:,s)\b;