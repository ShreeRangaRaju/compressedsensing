load baompfordiffmu1mu2

baomp1 = baOMPsuccessCount1/noSimulations;
baomp2 = baOMPsuccessCount2/noSimulations;
baomp3 = baOMPsuccessCount3/noSimulations;
baomp4 = baOMPsuccessCount4/noSimulations;
baomp5 = baOMPsuccessCount5/noSimulations;
bagiomp = bagiompsuccessCount/noSimulations;


plot(K,bagiomp,'r-*');
hold on;
plot(K,baomp1,'b-o');
hold on;
plot(K,baomp2,'g->')
hold on;
plot(K,baomp3,'m-<')
hold on;
plot(K,baomp4,'c-x')
hold on;
plot(K,baomp5,'k--')
grid on;
xlabel('K, Sparsity level')
ylabel('Probability of Reconstruction')
title('Exact Reconstruction rate of gaussian saprse signal (500 realizations): N = 256, M = 128')
%legend('bagiomp','baomp','OMP','BP','SP')