function [y]=mystomp(b,A,h)
% mystomp.m -- Stagewise Orthogonal Matching Pursuit
%==========================================================================
% Inputs
%   b   -   Input Signal(Generally Undersampled)
%   A   -   Dictionary or Measurement Matrix
%   h   -   Number of Iterations/Sparsity Level
%
%==========================================================================
% Outputs
%   y   -   Solution to the Equation b=A*x
%
%==========================================================================
% Comments
%   This is a direct implementation of Stagewise Orthogonal Matching Pursuit
%   It selects all the atoms greater than a set threshold 
%
%==========================================================================
%


ts = 2.3;  % Emperically determined value of ts
n = size(A);  % size of the measurement matrix
%a1 = zeros(n);
r = b;    % Initial value of residue is equal to the Input signal
m = sqrt(n(1,1));
threshold = ts*norm(r)/m;
i = 1;
n1 = norm(r);
%Ik= [];

while 1
    
   g = abs(A'*r);
   [ik,~] = find(g>threshold); % Selecting all the atoms greater than a set threshold
   
   y = pinv(A(:,ik))*b;
 
 
   r = b-A(:,ik)*y;
   n2 = norm(r);
   threshold = ts*n2/m;
   if n2 > n1 || i > h || n2 < 1e-5 % If the norm falls below a set value the functions breaks
       
       break;
   else n1=n2; i=i+1; 
   end
   
end


end
