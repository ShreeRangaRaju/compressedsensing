% final program for checking of algorithms and to run 500 simulations

clc; clear all; close all;

RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

N = 256;
K  = 45;
M = 100;


maxError = 1e-3;

noSimulations = 500;



%baOMPsuccessCount1 =  zeros(length(M),1);
baOMPsuccessCount1 =  zeros(length(K),1);
% baOMPsuccessCount3 =  zeros(length(K),1);
% baOMPsuccessCount4 =  zeros(length(K),1);
% baOMPsuccessCount5 =  zeros(length(K),1);
bagiompsuccessCount = zeros(length(K),1);
bagiomptestsuccessCount = zeros(length(K),1);
%  OMPsuccessCount = zeros(length(M),1);
%  SPsuccessCount1 = zeros(length(M),1);
%  BPsuccessCount = zeros(length(M),1);
%fbp_bin_successCount30 = zeros(length(K),1);
fbp_bin_successCount2013 = zeros(length(K),1);




%bagiomptestsuccessCount =  zeros(length(K),1);


% mtx = dctmtx(N);
% rr = randperm(N,M);
% A = mtx(rr,:);
A = randn(M,N);
for i = 1:N % normalizing the signal begins here
    r = A(:,i);
    nrm1 = norm(r);
    A(:,i) = r/nrm1;
end


for len = 1:length(K)
    k = K(len)
    
    
    
    for t = 1:noSimulations
        
        
        x=zeros(N,1); %initializing the signal
        position = randperm(N,k); %randomly chosen non-zero indices of x
        %originalpos = sort(position);
        x(position) = randn(k,1);
        % x(position) = 1;
        
        b = A*x;
        
        %baomp simulation1
%                mu1 = 0.4; mu2 = 0.6; baOmP_maxError = 10^(-6); maxIter = M;
%                [baOMP_estX, baOMP_estSupport] = baOMP(A, b, mu1, mu2, baOmP_maxError, maxIter);
%                err = max(abs(x-baOMP_estX));
%                if(err < maxError)
%                    baOMPsuccessCount1(len) = baOMPsuccessCount1(len) + 1;
%               end
        %
        
        %         %bagitest
% %                [bagiomptest_xest] = bagiomp(A,b,M);
% %               err = max(abs(x-bagiomptest_xest));
% %                if(err < maxError)
% %                    bagiompsuccessCount(len) = bagiompsuccessCount(len)+1;
% %         % %
% %                end
              
                [bagiomptest_xest1] = bagiomptest(A,b,k);
              err1 = max(abs(x-bagiomptest_xest1));
               if(err1 < maxError)
                   bagiomptestsuccessCount(len) = bagiomptestsuccessCount(len)+1;
        % %
               end
              if(err < err1)
                  t
                  
              end
        %
        %
        %
        %         %
%             alpha = 30; beta = 29;
%                  [xest,support] = fbp(A,b,alpha,beta);
%             err = max(abs(x-xest));
%            if(err < maxError)
%                 fbp_bin_successCount2013(len) = fbp_bin_successCount2013(len)+1;
%         % %
% %                 end
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %          %OMP Simulation
%         %                [xestOMP,suppotOMP] = OMP(A,b,k);
        %                 err = max(abs(x-xestOMP));
        %                  if(err < maxError)
        %                     OMPsuccessCount(len) = OMPsuccessCount(len) + 1;
        %                  end
        %
        %
        %               %SP simulation
        %                 [xestSP,supportSP] = SPska1(A,b,k);
        %                err = max(abs(x-xestSP));
        %                 if(err < maxError)
        %                     SPsuccessCount1(len) = SPsuccessCount1(len) + 1;
        %                 end
        %
        %            % BP simulation
        %                x0 = A'*b; % initial guess = minimum energy
        %                 BP_estX = l1eq_pd(x0, A, [], b, 10^(-3));
        %                 err = max(abs(x-BP_estX));
        %                 if(err < maxError)
        %                     BPsuccessCount(len) = BPsuccessCount(len) + 1;
        %                 end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
    end
    
end


%save('testfbpandbagiM1003029')
