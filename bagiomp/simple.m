clc; close all; clear all;
%tic;
%addpath ./noiselets
%RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

%rng('default')
N = 10; % dimesion of signal
K = 2; % level of sparsity
M = 4;
success = 0;

maxError = 10^-3;
maxiter = M;
no.simulations = 1;

A = randn(M,N);
for i = 1:N % normalizing the signal begins here
    r = A(:,i);
    nrm1 = norm(r);
    A(:,i) = r/nrm1;
end

% function handles
% theta = @(z) A_noiselet(z, (1:M));
% thetat = @(z) At_noiselet(z, (1:M), N);
% for i = 1:no.simulations
x=zeros(N,1); %initializing the signal
position = randperm(N,K); %randomly chosen non-zero indices of x
originalpos = sort(position);
x(position) = randn(K,1);
gix  = giniindex(x);
% x(position) = 1;
%x = [0 0 0 0 0 1.5887 0 0 0.1978 0]'
%  gix = giniindex(x);
%x = [1 0 0 0 0 1 0 0 0 0 1]'

% My values for x
% x = [0 0 -0.00001 0 -0.1989 0.000005 0.0003 1.0023 -0.00001 0.00002]';

%b = theta(x);
b = A*x;
%trans = A'*b;
% gitrans = giniindex(trans);
% diff = abs(gix-gitrans)
% nrmb = norm(b)
% SMNR = 15;
% signalStd = 1;
% noiseVar = (K/M)*(signalStd^2)*10^(-SMNR/10);
% noiseStd = sqrt(noiseVar);
% b = b + noiseStd*randn(M,1); % Measurements
%b1 = round(b);
% gib = giniindex(b);

%[Xestbaomp,estsupportbaomp] = baOMP(A, b, mu1, mu2, maxerror, maxiter);

[Xest,estsupport] = newgi(A,b,M,gix);
% if(abs(max(x-Xest)) < 0.001)
%     success = success + 1
% end
% x
% Xest


