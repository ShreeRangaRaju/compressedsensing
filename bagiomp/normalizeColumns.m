function B = normalizeColumns(A)
% Normalize each column of the given matrix to 1.
% B = NORMALIZE_COLUMNS(A).
% A is m x n input matrix.
% B is m x n output matrix with l2 norm of each column 1.
% i.e., ||b_i||_2 = 1.
% copied from :
% http://www.mathworks.com/matlabcentral/fileexchange/26416-function-to-normalize-columns-of-given-matrix/content/Normalize_Colmns.m


%
%
% Author      	: Sooraj K. Ambat
% Email       	: sooraj@ece.iisc.ernet.in/sooraj.k.ambat@gmail.com
% Address      	: Ph.D. Scholar,
%                 Statistical Signal Processing Lab,
%                 Indian Institute of Science, Bangalore, 560012, India.
% Date        	: 01 Dec 2011
% Last Modified	: 09 May 2012

% In the spirit of reproducible research, here we provide the Matlab codes
% used to generate the results given in our paper entitled "On Selection 
% of Search Space Dimension in Compressive Sampling Matching Pursuit" in 
% Proceedings of IEEE Region 10 Conference (TENCON), November 2012.
% Authors: Sooraj K. Ambat, Saikat Chatterjee, K.V.S. Hari.
% 
% If you use this code in your work, please cite our paper mentioned above.


% This script/program is released under the Commons Creative Licence
% with Attribution Non-commercial Share Alike (by-nc-sa)
% http://creativecommons.org/licenses/by-nc-sa/3.0/
% Short Disclaimer: this script is for educational purpose only.





B = A/(diag(sqrt(diag(A'*A))));
