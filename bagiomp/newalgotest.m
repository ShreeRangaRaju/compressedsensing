% final program for checking of algorithms and to run 500 simulations

clc; clear all;

RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

N = 256;
K  = 60:5:75;
M = 128;


maxError = 1e-3;

noSimulations = 500;




%cosampsuccessCount0 =  zeros(length(K),1);
%cosampsuccessCount =  zeros(length(K),1);
%baOMPsuccessCount =  zeros(length(K),1);
% baOMPsuccessCount4 =  zeros(length(K),1);
% baOMPsuccessCount5 =  zeros(length(K),1);
 bagiompsuccessCount = zeros(length(K),1);
% cosampsuccessCount1 = zeros(length(K),1);
%  OMPsuccessCount = zeros(length(M),1);
%  SPsuccessCount1 = zeros(length(M),1);
%  BPsuccessCount = zeros(length(M),1);
%fbp_bin_successCount30 = zeros(length(K),1);
%fbp_bin_successCount2013 = zeros(length(K),1);

% cardcosamp = zeros(length(K),1);
% cardcosamp0 = zeros(length(K),1);
% cardcosamp1 = zeros(length(K),1);





A = randn(M,N);
for i = 1:N % normalizing the signal begins here
    r = A(:,i);
    nrm1 = norm(r);
    A(:,i) = r/nrm1;
end


for len = 1:length(K)
    k = K(len)
    
    
    
    for t = 1:noSimulations
        
        
        x=zeros(N,1); %initializing the signal
        position = randperm(N,k); %randomly chosen non-zero indices of x
        originalpos = sort(position);
        x(position) = randn(k,1);
        % x(position) = 1;
        gix = giniindex(x);
        b = A*x;
        
%        baomp simulation1
%                mu1 = 0.4; mu2 = 0.6; baOmP_maxError = 10^(-6); maxIter = M;
%                 [baOMP_estX, baOMP_estSupport] = baOMP(A, b, mu1, mu2, baOmP_maxError, maxIter);
%                err = max(abs(x-baOMP_estX));
%                if(err < maxError)
%                    baOMPsuccessCount(len) = baOMPsuccessCount(len) + 1;
%               end
%         %
        
%         %         %bagitest
               [bagiomptest_xest] = newgi(A,b,M,gix);
              err = max(abs(x-bagiomptest_xest));
               if(err < maxError)
                   bagiompsuccessCount(len) = bagiompsuccessCount(len)+1;
        
               end
%               
%                 [xest,support] = CoSaMP(A,b,k);
%                 cardcosamp(len) = cardcosamp(len) + length(intersect(originalpos,support));
%               err1 = max(abs(x-xest));
%                if(err1 < maxError)
%                    cosampsuccessCount(len) = cosampsuccessCount(len)+1;
%         
%                end
               
%                %%%
%                [xest0,support0] = CoSaMP0(A,b,k);
%                 cardcosamp0(len) = cardcosamp0(len) + length(intersect(originalpos,support0));
% %               err1 = max(abs(x-xest0));
% %                if(err1 < maxError)
% %                    cosampsuccessCount0(len) = cosampsuccessCount0(len)+1;
% %         
% %                end
% %                
% %                %%%
%                [xest1,support1] = CoSaMP1(A,b,k);
%                 cardcosamp1(len) = cardcosamp1(len) + length(intersect(originalpos,support1));
% %               err1 = max(abs(x-xest1));
% %                if(err1 < maxError)
% %                    cosampsuccessCount1(len) = cosampsuccessCount1(len)+1;
% % %         
%               end
% % %               if(err < err1)
% %                   t
% %                   
% %               end
        %
        %
        %
        %         %
%             alpha = 30; beta = 29;
%                  [xest,support] = fbp(A,b,alpha,beta);
%             err = max(abs(x-xest));
%            if(err < maxError)
%                 fbp_bin_successCount2013(len) = fbp_bin_successCount2013(len)+1;
%         % %
% %                 end
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %          %OMP Simulation
%         %                [xestOMP,suppotOMP] = OMP(A,b,k);
        %                 err = max(abs(x-xestOMP));
        %                  if(err < maxError)
        %                     OMPsuccessCount(len) = OMPsuccessCount(len) + 1;
        %                  end
        %
        %
        %               %SP simulation
        %                 [xestSP,supportSP] = SPska1(A,b,k);
        %                err = max(abs(x-xestSP));
        %                 if(err < maxError)
        %                     SPsuccessCount1(len) = SPsuccessCount1(len) + 1;
        %                 end
        %
        %            % BP simulation
        %                x0 = A'*b; % initial guess = minimum energy
        %                 BP_estX = l1eq_pd(x0, A, [], b, 10^(-3));
        %                 err = max(abs(x-BP_estX));
        %                 if(err < maxError)
        %                     BPsuccessCount(len) = BPsuccessCount(len) + 1;
        %                 end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
    end
    
end

plot(K,bagiompsuccessCount/noSimulations,'b')
% hold on;
% plot(K,cosampsuccessCount0/noSimulations,'m')
% hold on;
% plot(K,cosampsuccessCount1/noSimulations,'r')
 grid on;
% legend('OrigCosamp','CosampGI(matchfilter)','CosampGI(residue)')
% xlabel 'K'
% ylabel 'Probability of exact reconstruction'
% title 'Exact reconstruction rate of gaussian sparse signal (500 realizations): N = 256, M = 100'
% 
 %save('modifiedginicosampASCE');
