function [xest] = OMP_fnhandle_lsqr( A, b, k,N )

if iscell(A)
    largescale = true;
    Af = A{1}
    At = A{2}
end

% initialization

RHS = b
Ik = []
M = size(RHS,1)
xest = zeros(N,1)

%lsqr parameters
cg_tol = 1.0e-3;
cg_maxit = 50;

if largescale
    LSQR_ALG    = @(RHS,Afcn,x_warmstart) lsqr(Afcn,RHS,cg_tol,cg_maxit,[],[],x_warmstart )
end

for i = 1:k
    
    Ar = At(RHS)
    
    [~, maxind] = max(abs(Ar))
    
    Ik = union(Ik,maxind)
    
    x_warmstart = xest(Ik)
    
    

    
    if largescale
    Afcn = @(x,version) partialA( N, Ik, Af, At, x, version )
    [x_T,~] = LSQR_ALG(RHS,Afcn,x_warmstart)
    end
    
    xest = 0*xest
    xest(Ik) = x_T;
    
    RHS = b-Af(xest)
end

end
   

function z = partialA( N, Ik, Af, At, x, version )

x
switch lower(version)
    case 'notransp'
        %if (transp_flag == 'notransp')
        %zero-pad:
        
        y   = zeros(N,1)
        y(Ik)    = x
        z       = Af(y)
        
    case 'transp'
        %elseif (transp_flag == 'transp')
        
        y   = At(x)
        % truncate:
        z   = y(Ik)
end
end % -- end of subfuction

