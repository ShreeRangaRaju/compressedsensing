    function [xest,r] = OMP_fnhandle_test( A, b, k )

if iscell(A)
    LARGESCALE  = true
    Af  = A{1}
    At  = A{2}
    
end

% Intitialize 

r = b;

Ar = At(r);
N = size(Ar,1);       % number of atoms
M = size(r,1) ;       % size of atoms

unitVector     = zeros(N,1);
xest           = zeros(N,1);


A_T         = zeros(M,k);
A_T_nonorth = zeros(M,k);

for kk = 1:k
    
    [~,ind_new]     = max(abs(Ar));
    indx_set(kk)    = ind_new;
    
    if LARGESCALE
        unitVector(ind_new)     = 1;
        atom_new                = Af( unitVector );
        unitVector(ind_new)     = 0;
    end
    
    A_T_nonorth(:,kk)   = atom_new    % before orthogonalizing and such
    
    % First, orthogonalizing atom_new using gram-shmidt method
    for j = 1:(kk-1)
        atom_new    = atom_new - (atom_new'*A_T(:,j))*A_T(:,j)
    end
    
    % second, normalize:
    atom_new        = atom_new/norm(atom_new);
    A_T(:,kk)       = atom_new
    
    % third, solve least-squares problem (which is now very easy
    %   since A_T(:,1:kk) is orthogonal )
    xest_T     = A_T(:,1:kk)'*b
    
    % fourth, update residual:
    r = b - A_T(:,1:kk)*xest_T;
    
    if kk < k
        Ar  = At(r); % prepare for next round
    end
    
    % For the last iteration, we need to do this without orthogonalizing A
    % so that the x coefficients match what is expected.
    xest_T = A_T_nonorth(:,1:kk)\b; %equivalent to pinv
    xest( indx_set(1:kk) )   = xest_T
    
end