function [estX, estSupport] = OMP(A,b,K)

r  = b;
I = [];
k = 0;
for  k = 1:K
    
    trans = A'*r; % taking the transpose of A and multiplying with r
    abso = abs(trans); % taking the absolute value
    [maxVal, index] = max(abso); % getting the indices of the maximum value
    i = index; % storing the indices in i
    I = union(I,i); % taking the union of Io and i
    H = A(:,I);
     xx = pinv(H)*b; % taking the pseudo inverse of A and multiplying with b
    r = b-H*xx;    
end


[M,N] = size(A);
estX = zeros(N,1);
estX(I) = xx;
estSupport = I;
     end


    