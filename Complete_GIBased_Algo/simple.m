clc; close all; clear all;
%tic;
%addpath ./noiselets
%RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

%rng('default')
N = 10; % dimesion of signal
K = 4; % level of sparsity
M = 7;
success = 0;

maxError = 10^-3;
maxiter = M;
no.simulations = 1;

A = randn(M,N);
for i = 1:N % normalizing the signal begins here
    r = A(:,i);
    nrm1 = norm(r);
    A(:,i) = r/nrm1;
end


x=zeros(N,1); %initializing the signal
position = randperm(N,K) %randomly chosen non-zero indices of x
originalpos = sort(position);
x(position) = randn(K,1);
gix  = giniindex(x);

b = A*x;

% SMNR = 15;
% signalStd = 1;
% noiseVar = (K/M)*(signalStd^2)*10^(-SMNR/10);
% noiseStd = sqrt(noiseVar);
% b = b + noiseStd*randn(M,1); % Measurements
%b1 = round(b);
% gib = giniindex(b);

%[Xestbaomp,estsupportbaomp] = baOMP(A, b, mu1, mu2, maxerror, maxiter);
x;
% Xest = SolveStOMP(A,b,N,'FAR',0.5,M,0,1e-5)
Xest = newgi(A,b,M,gix);

