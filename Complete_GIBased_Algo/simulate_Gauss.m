    


clear all; close all; clc

algoName = 'Modified_CoSaMP'; % name of the algorithm
sourceType =  'BernoulliRndSign';

alpha = 0.10:0.01:0.20;

% SMNR = 15 ; %(in dB)




% for repeatability of the experiment
RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));


%% signal generation
N = 500; % signal dimension
K = 20; % sparsity level
S = 3;
T = 3;


%% initialization
xL2NormSquareSum =  zeros(length(alpha),1);

% CoSaMP
supportCardinality_CoSaMP = zeros(length(alpha),1);
xDiffL2NormSquareSum_CoSaMP =  zeros(length(alpha),1);
noIter_CoSaMP =  zeros(length(alpha),1);

err = zeros(T,S,length(alpha));
err_modified_cosamp = zeros(T,S,length(alpha));
err_modified_gicosamp = zeros(T,S,length(alpha));


%mu = [0.5 0.6 0.7 0.8 0.9]; % different values for \mu

% Modified_CoSaMP
supportCardinality_Modified_CoSaMP = zeros(length(alpha),1);
xDiffL2NormSquareSum_Modified_CoSaMP = zeros(length(alpha),1);
noIter_Modified_CoSaMP =  zeros(length(alpha),1);

% Modified_GiCoSaMP
supportCardinality_Modified_GICoSaMP = zeros(length(alpha),1);
xDiffL2NormSquareSum_Modified_GICoSaMP = zeros(length(alpha),1);
noIter_Modified_GICoSaMP =  zeros(length(alpha),1);





for i=1:length(alpha)
    fprintf(1, '\n alpha=%2.2f \n', alpha(i));
    
    M = round(alpha(i)*N); % no. of sensors
    for s=1:S
        % Generate Measurement matrix
        fprintf(1, 's= ');
        fprintf(1, '%d,', s);
        A = generateMeasurementMatrix('NormalizedStdGaussian', M, N);
        for t=1:T
            %             fprintf(1, '%d, ', t);
            [x, nonZeroPos]= generateSignal(sourceType, N, K);
            gix = giniindex(x);
            b = A*x;
            if(exist('SMNR', 'var'))%add noise
                signalStd = 1;
                noiseVar = (K/M)*(signalStd^2)*10^(-SMNR/10);
                noiseStd = sqrt(noiseVar);
                b = b + noiseStd*randn(M,1); % Measurements
            end
            actualPosition = sort(nonZeroPos);
            xL2NormSquareSum(i) = xL2NormSquareSum(i) + (norm(x,2))^2;%for SRNR
            
            
            %% CoSaMP
            [xk_CoSaMP, Ik_CoSaMP, noIter] = CoSaMP(A, b,K);
            err(t,s,i) = max(abs(x-xk_CoSaMP))
            xDiffL2NormSquareSum_CoSaMP(i) = xDiffL2NormSquareSum_CoSaMP(i) + (norm(x-xk_CoSaMP, 2))^2; %for SRNR
            supportCardinality_CoSaMP(i)=  supportCardinality_CoSaMP(i) + length(intersect(actualPosition,Ik_CoSaMP));%for ASCE
            noIter_CoSaMP(i) = noIter_CoSaMP(i) + noIter;
            
            
            %% Modified_CoSaMP
            
            
            [xk_Modified_CoSaMP, Ik_Modified_CoSaMP, noIter1] = Modified_CoSaMP(A, b, K, 0.6);
            err_modified_cosamp(t,s,i) = max(abs(x-xk_Modified_CoSaMP));
            xDiffL2NormSquareSum_Modified_CoSaMP(i) = xDiffL2NormSquareSum_Modified_CoSaMP(i) + (norm(x-xk_Modified_CoSaMP, 2))^2; %for SRNR
            supportCardinality_Modified_CoSaMP(i)=  supportCardinality_Modified_CoSaMP(i) + length(intersect(actualPosition,Ik_Modified_CoSaMP));%for ASCE
            noIter_Modified_CoSaMP(i) = noIter_Modified_CoSaMP(i) + noIter1;
            
            
            
            
            %% Modified GI CoSaMP
            
            [xk_Modified_GICoSaMP, Ik_Modified_GICoSaMP, noIter1] = gicosamp(A, b, K, gix);
            err_modified_gicosamp(t,s,i) = max(abs(x-xk_Modified_GICoSaMP));
            xDiffL2NormSquareSum_Modified_GICoSaMP(i) = xDiffL2NormSquareSum_Modified_GICoSaMP(i) + (norm(x-xk_Modified_GICoSaMP, 2))^2; %for SRNR
            supportCardinality_Modified_GICoSaMP(i)=  supportCardinality_Modified_GICoSaMP(i) + length(intersect(actualPosition,Ik_Modified_GICoSaMP));%for ASCE
            noIter_Modified_GICoSaMP(i) = noIter_Modified_GICoSaMP(i) + noIter1;
            
            
            
        end
    end
    

end

% resultsMatFileFolder = 'resultMatFiles';
% if(~exist(resultsMatFileFolder, 'dir'))
% 	mkdir(resultsMatFileFolder); % create a new Directory for storing results
% end
%
% if(exist('SMNR', 'var'))
% 	outFileName = [resultsMatFileFolder, '/', algoName, '_', sourceType,'SMNR', num2str(SMNR), 'dB_AllAlpha.mat']
% 	save(outFileName, 'N', 'K', 'M', 'S', 'T', 'alpha', '*supportCardinality*', '*L2NormSquare*', 't', 's', 'algoName', 'noIter*', 'SMNR', 'mu');
% else
% 	outFileName = [resultsMatFileFolder, '/', algoName, '_',  sourceType,'_AllAlpha.mat']
% 	save(outFileName, 'N', 'K', 'M', 'S', 'T', 'alpha', '*supportCardinality*', '*L2NormSquare*', 't', 's', 'algoName', 'noIter*', 'mu');
% end

save('cosampgimodificationS100T100_rademacher')

%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
