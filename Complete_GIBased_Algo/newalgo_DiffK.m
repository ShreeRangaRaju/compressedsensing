
% final program for checking of algorithms and to run 500 simulations

clc; clear all; close all;
tic;

RandStream.setDefaultStream(RandStream('mt19937ar','seed',0));

N = 256;
K  = 10:10:90;
M = 128;
noSimulations = 500;
maxError = 10^(-3);

% xl2normsquaresum = zeros(length(M),1);
%
% xdiffl2normsquare_omp = zeros(length(M),1);
% xdiffl2normsquare_sp = zeros(length(M),1);
% xdiffl2normsquare_bp = zeros(length(M),1);
% % xdiffl2normsquare_stomp = zeros(length(M),1);
% % xdiffl2normsquare_romp = zeros(length(M),1);
% xdiffl2normsquare_gi = zeros(length(M),1);

ProbsuccessGI = zeros(length(K),1);
ProbsuccessOMP = zeros(length(K),1);
ProbsuccessSP = zeros(length(K),1);
ProbsuccessBP = zeros(length(K),1);
ProbsuccessSTOMP = zeros(length(K),1);
ProbsuccessROMP = zeros(length(K),1);

 A = randn(M,N);
    for i = 1:N % normalizing the signal begins here
        r = A(:,i);
        nrm1 = norm(r);
        A(:,i) = r/nrm1;
    end


for len = 1:length(K)
    k = K(len)
    
    for t = 1:noSimulations
        fprintf(1, 't= ');
        fprintf(1, '%d,', t);
        x=zeros(N,1); %initializing the signal
        position = randperm(N,k); %randomly chosen non-zero indices of x
        x(position) = randn(k,1);
        %x(position) = 1;
        gix  = giniindex(x);
        %         xl2normsquaresum(len) = xl2normsquaresum(len) + (norm(x))^2;
        
        
        
        
        b = A*x;
        % TO ADD NOISE
        %         SMNR = 15;
        %         signalStd = 1;
        %         noiseVar = (k/M)*(signalStd^2)*10^(-SMNR/10);
        %         noiseStd = sqrt(noiseVar);
        %         b = b + noiseStd*randn(M,1); % Measurements
        
        %newgitest
        [xestgi] = newgi(A,b,M,gix);
        err = max(abs(x-xestgi));
        if(err < maxError)
            ProbsuccessGI(len) = ProbsuccessGI(len)+1;
        end
        %         xdiffl2normsquare_gi(len) = xdiffl2normsquare_gi(len) + (norm(x-xestgi))^2;
        
        
        %OMP Simulation
        [xestOMP] = OMP(A,b,k);
        err = max(abs(x-xestOMP));
        if(err < maxError)
            ProbsuccessOMP(len) = ProbsuccessOMP(len)+1;
        end
        
        %         xdiffl2normsquare_omp(len) = xdiffl2normsquare_omp(len) + (norm(x-xestOMP))^2;
        
        
        
        %SP simulation
        [xestSP] = SPska1(A,b,k);
        err = max(abs(x-xestSP));
        if(err < maxError)
            ProbsuccessSP(len) = ProbsuccessSP(len)+1;
        end
        
        %         xdiffl2normsquare_sp(len) = xdiffl2normsquare_sp(len) + (norm(x-xestSP))^2;
        
        
        % BP simulation
        x0 = A'*b; % initial guess = minimum energy
        BP_estX = l1eq_pd(x0, A, [], b, 10^(-3));
        err = max(abs(x-BP_estX));
        if(err < maxError)
            ProbsuccessBP(len) = ProbsuccessBP(len)+1;
        end
        
        %         xdiffl2normsquare_bp(len) = xdiffl2normsquare_bp(len) + (norm(x-BP_estX))^2;
        
        
        %ROMP Simulation
        [xestromp] = romp(k,A,b);
        err = max(abs(x-xestromp));
        if(err < maxError)
            ProbsuccessROMP(len) = ProbsuccessROMP(len)+1;
        end
        
        %         xdiffl2normsquare_romp(len) = xdiffl2normsquare_romp(len) + (norm(x-xestromp))^2;
        
        %
        %         %STOMP Simulation
        %         [xeststomp] = SolveStOMP(A,b,N,'FAR',0.5,M,0,1e-5);
        %         err = max(abs(x-xeststomp));
        %         if(err < maxError)
        %             ProbsuccessSTOMP(len) = ProbsuccessSTOMP(len)+1;
        %         end
        
        %         xdiffl2normsquare_stomp(len) = xdiffl2normsquare_stomp(len) + (norm(x-xeststomp))^2;
        
        
        
    end
end

% SRERgi = 10*log10(xl2normsquaresum./xdiffl2normsquare_gi);
% SRERbp = 10*log10(xl2normsquaresum./xdiffl2normsquare_bp);
% SRERomp = 10*log10(xl2normsquaresum./xdiffl2normsquare_omp);
% SRERsp = 10*log10(xl2normsquaresum./xdiffl2normsquare_sp);
%
save('GaussianBAOMPSetupDiffK')
toc;


