load('GaussBAOMPSetupDiffM')
plot(M,ProbsuccessGI/noSimulations,'r-*');
hold on;
plot(M,ProbsuccessBP/noSimulations,'m-o');
hold on;
plot(M,ProbsuccessOMP/noSimulations,'g->');
hold on;
plot(M,ProbsuccessROMP/noSimulations,'k-d');
hold on;
plot(M,ProbsuccessSP/noSimulations,'b-s');
grid on;
xlabel 'No. of Measurements, M'
ylabel 'Probability of Reconstruction'
title 'Exact reconstruction rate of Gaussian sparse signal (500 realizations): N = 256, M =128'
legend('GIMP','BP','OMP','ROMP','SP')