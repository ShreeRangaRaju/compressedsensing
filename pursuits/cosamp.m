function[Xest,support] = cosamp(A, b, K)

r0 = b;
iter = 1;
I0 = [];
currentNorm = norm(r0);
[M,N] = size(A);


while 1
    
   iter = iter+1;
   prevI0 = I0;
   
   [maxVal, maxInd] = sort(abs(pinv(A)*r0),'descend');
   
   I0 = union(I0,maxInd(1:2*K));
   
   vTow = zeros(N,1);
   vTow(IO) = pinv(A(:,IO))*b;
   
   [maxVal1, maxInd1] = sort(abs(vTow),'descend');
   I0 = maxInd1(1:K);
   
   xhat = zeros(N,1);
   xhat(I0) = vTow(I0);
   
   r0 = b-A*xhat;
   
   previousNorm = currentNorm;
   currentNorm = norm(r0);
   
   if (currentNorm >= previousNorm)
       I0 = prevI0;
      break;
   end
   
  
  
   
end


Xest = zeros(N,1);
Xest = xhat;
support = I0;
