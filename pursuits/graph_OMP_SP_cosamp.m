clc; close all;
load testomp; load testsp; load testcosamp;
figure(1)
plot((m1:10:m2),g1,'ro-')
hold on;
plot((m1:10:m2),g2,'m*--')
hold on;
plot((m1:10:m2),g3,'x:')
l1 = legend('OMP','SP','CoSamp')
title('ratio graph - N = 500, M = 50:10:100, x = 500, K = 20')
xlabel('M')
ylabel('SRNR')

figure(2)
plot((m1:10:m2),u1,'ro-')
hold on;
plot((m1:10:m2),u2,'m*--')
hold on;
plot((m1:10:m2),u3,'x:')
l2 = legend('OMP','SP','CoSamp')
title('ASCE - N = 500, M = 50:10:100, x = 500, K = 20')
xlabel('M')
ylabel('ASCE')
