function [Xest,support] = SP(A, b, K)


iter = 0;
[M,N] = size(A);

abso = abs(A'*b); 
[maxVal,maxInd] = sort(abso,'descend');
I0 = (maxInd(1:K));

r0 = b-(A(:,I0)*pinv(A(:,I0))*b);
currentNorm = norm(r0);


while 1 

    iter = iter +1;
    prevI0 = I0;
    abso2 = abs(A'*r0);
    [maxVal2, maxInd2] = sort(abso2,'descend');
     IO = union(I0,maxInd2(1:K));
     
     vTow = zeros(N,1);
     vTow(IO) = pinv(A(:,IO))*b;
     [maxVal3, maxInd3] = sort(abs(vTow),'descend');
    I0 = maxInd3(1:K);
    
    r0 = b-(A(:,I0)*pinv(A(:,I0))*b);
    
    prevNorm = currentNorm;
    currentNorm = norm(r0);
   
    if (prevNorm <= currentNorm)
        I0 = prevI0;
        break;
    end
end


h = pinv(A(:,I0))*b;
Xest = zeros(N,1);
Xest(I0) = h;
support = I0;
end
    